Rails.application.routes.draw do
  
  get 'account_settings/account_settings'
  resources :categories
  resources :roles
	get 'about' => 'welcome#about'
	get 'contact' => 'welcome#contact'
	get 'post_novi' => 'posts#new'

	patch 'subscribe' => "account_settings#subs_true"
	patch 'unsubscribe' => "account_settings#subs_false"

	get 'account_settings' => 'account_settings#account_settings'
  get 'drafts' => 'drafts#drafts'
  
  resources :comments
  devise_for :users
  resources :posts

  root to: 'categories#index'
end

class AddSubsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :subs, :boolean
  end
end

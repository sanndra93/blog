class ChangeColumnSubsDefault < ActiveRecord::Migration[5.2]
  def change
  	change_column_default(:users, :subs, true)
  end
end

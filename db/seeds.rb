# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

if !Role.any?
	Role.create(name:"admin")
	Role.create(name:"moderator")
	Role.create(name: "reader")
end

if !User.any?
	User.create(email: "andrijana@gmail.com", password: "123456", :role_id => Role.find(1).id)
end


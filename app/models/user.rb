class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  after_create :send_signup_email

  def send_signup_email
  	UserNotifierMailer.send_signup_email(self).deliver_now
  end

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  belongs_to :role
  has_many :posts, dependent: :destroy

    has_attached_file :avatar, styles: { medium: "300x300", thumb: "100x100" }
    validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/



end

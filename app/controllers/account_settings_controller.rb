class AccountSettingsController < ApplicationController
  def account_settings
  end

  def subs_true
  	@user = User.find(current_user.id)
  	@user.subs = true
  	@user.save
  	redirect_back(fallback_location: root_path)
  end

  def subs_false
  	@user = User.find(current_user.id)
  	@user.subs = false
  	@user.save
  	redirect_back(fallback_location: root_path)
  end

  
end

